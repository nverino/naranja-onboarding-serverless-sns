const DYNAMODB = require("aws-sdk/clients/dynamodb");

const dbClient = new DYNAMODB({
  region: "us-east-1",
});

const clientsTable = process.env.CLIENTS_TABLE;

exports.handler = async (event) => {
  console.log('event', event);

  const queueItems = event.Records.map((record) => record.body);

  for (const item of queueItems) {
    const message = JSON.parse(item);
    const body = JSON.parse(message.Message);

    const cardNumber = `${_getRandomNumber(0000,9999)}-${_getRandomNumber(0000,9999)}-${_getRandomNumber(0000,9999)}-${_getRandomNumber(0000,9999)}`;
    const expirationDate = `${_getRandomNumber(01,12)}/${_getRandomNumber(22,30)}`;
    const ccv = `${_getRandomNumber(000,999)}`;

    let type = _calculateAge(body.birth) > 45 ? 'gold' : 'classic';

    const dbParams = {
      ExpressionAttributeNames: {
        "#card": "creditCard",
      },
      ExpressionAttributeValues: {
        ":card": {
          M: {
            "number": {
              S: cardNumber,
            },
            "expiration": {
              S: expirationDate,
            },
            "ccv": {
              S: ccv,
            },
            "type":{
              S: type
            }
          },
        },
      },
      Key: {
        dni: {
          S: body.dni,
        },
      },
      TableName: clientsTable,
      UpdateExpression: "SET #card = :card"
    };

    console.log('dbParams', dbParams);

    try {
      const dbResult = await dbClient.updateItem(dbParams).promise();
      console.log('dbResult', dbResult);
    } catch (error) {
      console.error(error);
      return {
        statusCode: 500,
        body: error,
      };
    }
  }

  return {
    statusCode: 200,
    body: "success",
  };
};

/**
 * Reference: https://www.joshwcomeau.com/snippets/javascript/random/
 * @param {*} min 
 * @param {*} max 
 * @returns 
 */
function _getRandomNumber(min, max){
  return Math.floor(Math.random() * (max - min) + min);
}

/**
 * Reference: https://stackoverflow.com/questions/4060004/calculate-age-given-the-birth-date-in-the-format-yyyymmdd
 */ 
 function _calculateAge(date) { 
    const birthdate = new Date(date);
    const diff = Date.now() - birthdate.getTime();
    const ageDate = new Date(diff);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}