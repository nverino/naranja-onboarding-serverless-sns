const DYNAMODB = require("aws-sdk/clients/dynamodb");
const SNS = require("aws-sdk/clients/sns");

const dbClient = new DYNAMODB({
  region: "us-east-1",
});

const snsClient = new SNS({
  region: "us-east-1",
});

const topicArn = process.env.SNS_TOPIC_ARN;
const clientsTable = process.env.CLIENTS_TABLE;

exports.handler = async (event) => {
  console.log('event', event);

  const body = JSON.parse(event.body);

  if (!body.dni || !body.name || !body.lastName || !body.birthdate) {
    return {
      statusCode: 400,
      body: "Incorrect properties",
    };
  }

  const age = _calculateAge(body.birthdate);

  if (age < 18 || age > 65) {
    return {
      statusCode: 400,
      body: "Client must be older than 18 years old and younger than 65 years old",
    };
  }

  const dbParams = {
    Item: {
      dni: {
        S: body.dni,
      },
      name: {
        S: body.name,
      },
      lastName: {
        S: body.lastName,
      },
      birthdate: {
        S: body.birthdate,
      },
    },
    TableName: clientsTable
  };

  const snsParams = {
    Message: JSON.stringify(body),
    TopicArn: topicArn,
  };

  try {
    const dbResult = await dbClient.putItem(dbParams).promise();
    console.log('dbResult', dbResult);
    const snsResult = await snsClient.publish(snsParams).promise();
    console.log('snsResult', snsResult);
  } catch (error) {
    console.error(error);
    return {
      statusCode: 500,
      body: error
    };
  }

  return {
    statusCode: 200,
    body: "Your request was processed successfully",
  };
};

/**
 * Reference: https://stackoverflow.com/questions/4060004/calculate-age-given-the-birth-date-in-the-format-yyyymmdd
 */ 
function _calculateAge(date) {
    const birthdate = new Date(date);
    const diff = Date.now() - birthdate.getTime();
    const ageDate = new Date(diff);
    return Math.abs(ageDate.getUTCFullYear() - 1970);
}