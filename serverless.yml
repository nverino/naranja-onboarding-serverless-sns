service: serverless

plugins:
  - serverless-iam-roles-per-function

provider:
  name: aws
  runtime: nodejs14.x
  stage: dev
  region: us-east-1

functions:
  createClient:
    handler: createClient.handler
    name: ${self:service}-${self:provider.stage}-create-client
    iamRoleStatements:
      - Effect: Allow
        Action:
          - dynamodb:PutItem
        Resource: !GetAtt ClientsDynamoDbTable.Arn
      - Effect: Allow
        Action:
          - sns:Publish
        Resource: !Ref ClientCreatedTopic
    events:
      - http:
          path: /client
          method: post
          cors: true
    environment:
      CLIENTS_TABLE: !Ref ClientsDynamoDbTable
      SNS_TOPIC_ARN: !Ref ClientCreatedTopic

  createCard:
    handler: createCard.handler
    name: ${self:service}-${self:provider.stage}-create-card
    iamRoleStatements:
      - Effect: Allow
        Action:
          - dynamodb:UpdateItem
        Resource: !GetAtt ClientsDynamoDbTable.Arn
    events:
      - sqs:
          arn: !GetAtt CreateCardQueue.Arn
    environment:
      CLIENTS_TABLE: !Ref ClientsDynamoDbTable

  createGift:
    handler: createGift.handler
    name: ${self:service}-${self:provider.stage}-create-gift
    iamRoleStatements:
      - Effect: Allow
        Action:
          - dynamodb:UpdateItem
        Resource: !GetAtt ClientsDynamoDbTable.Arn
    events:
      - sqs:
          arn: !GetAtt CreateGiftQueue.Arn
    environment:
      CLIENTS_TABLE: !Ref ClientsDynamoDbTable

resources:
  Resources:
    ClientsDynamoDbTable:
      Type: AWS::DynamoDB::Table
      # DeletionPolicy: Retain
      Properties:
        TableName: ${self:service}-${self:provider.stage}-clients
        AttributeDefinitions:
          -
            AttributeName: "dni"
            AttributeType: "S"   
        KeySchema:
          -
            AttributeName: "dni"
            KeyType: "HASH"
        ProvisionedThroughput:
          ReadCapacityUnits: 1
          WriteCapacityUnits: 1
        StreamSpecification:
          StreamViewType: "NEW_AND_OLD_IMAGES"

    ClientCreatedTopic:
      Type: AWS::SNS::Topic
      Properties:
        TopicName: ${self:service}-${self:provider.stage}-client-created-topic
        DisplayName: clientCreatedTopic
        Subscription:
          - Protocol: sqs
            Endpoint: !GetAtt CreateCardQueue.Arn
          - Protocol: sqs
            Endpoint: !GetAtt CreateGiftQueue.Arn

    CreateCardQueue:
      Type: "AWS::SQS::Queue"
      Properties:
        QueueName: ${self:service}-${self:provider.stage}-create-card-queue

    CreateCardQueuePolicy: 
      Type: AWS::SQS::QueuePolicy
      Properties: 
        Queues:
          - !Ref CreateCardQueue
        PolicyDocument: 
          Statement: 
            - Action: 
                - "SQS:*"
              Effect: "Allow"
              Resource: !GetAtt CreateCardQueue.Arn
              Principal:  
                AWS: 
                  - !Ref "AWS::AccountId"
            - Action: 
                - "SQS:SendMessage"
              Effect: "Allow"
              Resource: !GetAtt CreateCardQueue.Arn
              Condition:
                ArnLike:
                  "aws:SourceArn":
                    - !Ref ClientCreatedTopic
              Principal:  
                AWS:
                  - "*"

    CreateGiftQueue:
      Type: "AWS::SQS::Queue"
      Properties:
        QueueName: ${self:service}-${self:provider.stage}-create-gift-queue

    CreateGiftQueuePolicy: 
      Type: AWS::SQS::QueuePolicy
      Properties: 
        Queues: 
          - !Ref CreateGiftQueue
        PolicyDocument: 
          Statement: 
            -
              Action: 
                - "SQS:*"
              Effect: "Allow"
              Resource: !GetAtt CreateGiftQueue.Arn
              Principal:  
                AWS: 
                  - !Ref "AWS::AccountId"
            - 
              Action: 
                - "SQS:SendMessage"
              Effect: "Allow"
              Resource: !GetAtt CreateGiftQueue.Arn
              Condition:
                ArnLike:
                  "aws:SourceArn": 
                    - !Ref ClientCreatedTopic
              Principal:  
                AWS: 
                  - "*"
